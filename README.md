# NOMAD-Summer-2023 exercises on HPC basics (Erwin Laure & Markus Rampp, MPCDF)

## Part 1: Exploring a (newly conquered) HPC system: *Where do my computations run?*

### 1.1) Explore Hardware

#### Try basic linux tools (on LUMI login and compute node), compare with your laptop/PC: 

```
numactl --hardware

lstopo --no-io --of ascii 

cpuinfo #part of Intel-MPI

```


Results: 

```
lstopo --of svg #executed on login node!

#scp to your laptop and use inkview (svg) or alike
```

![](images/lumi_login_lstopo.svg)


### Basic pinning diagnostics for applications (LUMI / Slurm)

Try (on the compute nodes, submit a random MPI sample): 

```
srun --cpu-bind=verbose ...

export OMP_DISPLAY_AFFINITY=TRUE
#export OMP_AFFINITY_FORMAT= … # cf. OpenMP 5.0 spec 6.14 

srun --label ...
```

### 1.2) Run C or Fortran version of a portable pinning diagnostics library:

#### *Portable (and human-readable)* pinning diagnostics library **in C** by MPCDF (pincheck) 
      
```      
git clone https://gitlab.mpcdf.mpg.de/khr/pincheck.git

cd pincheck

module load gcc

make USE_GCC=1
```

  * Prepare and submit a hybrid MPI+OpenMP batch job on two nodes:	https://docs.lumi-supercomputer.eu/runjobs/scheduled-jobs/lumic-job/

  * note the different reservations for Wed, Fri, Sat, Sun: https://github.com/csc-training/nomad-school-paphos/blob/main/lumi-instructions.md

  * interpret results

  * Try with : `export OMP_PLACES=cores`, `export OMP_PLACES=threads`


#### *Portable (and human-readable)* pinning diagnostics library **in Fortran** by MPCDF (fpincheck) 

```      
git clone https://gitlab.mpcdf.mpg.de/mjr/fpincheck.git

cd fpincheck

module load gcc

make FC=gfortran MPIFC=mpif90 MPILD=mpif90
```

 * Prepare and submit a hybrid MPI+OpenMP batch job on two nodes:	https://docs.lumi-supercomputer.eu/runjobs/scheduled-jobs/lumic-job/

 * note the different reservations for Wed, Fri, Sat, Sun: https://github.com/csc-training/nomad-school-paphos/blob/main/lumi-instructions.md

 * interpret results

 * Try with : `export OMP_PLACES=cores`, `export OMP_PLACES=threads`


#### *Optional:* run or integrate it with your favourite application code 

Two options:

a) do no (or cannot) touch your application code

```
srun ./pincheck # C example
#alternatively: srun ./pinning_mpi.x # Fortran example

srun ./a.out
```

b) integrate into your application code (use our [f]pincheck source codes) and call after MPI-init

  * recommendation: every production run should always report “pinning” by default
    (cf. FHI-aims, …)
  * why?: pinning is highly specific to HPC system and environment, getting it wrong is **THE source of performance-loss and (silent) performance regressions**

  * Real-world example: standard output from a [hybrid MPI/OpenMP code](https://gitlab.mpcdf.mpg.de/mjr/nscouette):

```
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! NSCouette, a HPC code for DNS of Taylor-Couette flow                 !
!                                                                      !
! Copyright (C) 2016 Marc Avila, Bjoern Hof, Jose Manuel Lopez,        !
!                    Markus Rampp, Liang Shi                           !
!                                                                      !
! NSCouette is free software: you can redistribute it and/or modify    !
! it under the terms of the GNU General Public License as published by !
! the Free Software Foundation, either version 3 of the License, or    !
! (at your option) any later version.                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

starting NSCouette (5a419534) using   64 MPI tasks,    9 OpenMP threads/task

   TASKS:  64 THREADS: 9    THIS:  42     Host:adag029    Cores:  18  19  20  21  22  23  24  25  26
   TASKS:  64 THREADS: 9    THIS:  43     Host:adag029    Cores:  27  28  29  30  31  32  33  34  35
   TASKS:  64 THREADS: 9    THIS:   7     Host:adag012    Cores:  63  64  65  66  67  68  69  70  71
   TASKS:  64 THREADS: 9    THIS:   0     Host:adag012    Cores:   0   1   2   3   4   5   6   7   8
   TASKS:  64 THREADS: 9    THIS:   1     Host:adag012    Cores:   9  10  11  12  13  14  15  16  17
   TASKS:  64 THREADS: 9    THIS:   3     Host:adag012    Cores:  27  28  29  30  31  32  33  34  35
   TASKS:  64 THREADS: 9    THIS:   2     Host:adag012    Cores:  18  19  20  21  22  23  24  25  26
   TASKS:  64 THREADS: 9    THIS:   5     Host:adag012    Cores:  45  46  47  48  49  50  51  52  53
   TASKS:  64 THREADS: 9    THIS:   4     Host:adag012    Cores:  36  37  38  39  40  41  42  43  44
   TASKS:  64 THREADS: 9    THIS:   6     Host:adag012    Cores:  54  55  56  57  58  59  60  61  62
   TASKS:  64 THREADS: 9    THIS:  12     Host:adag013    Cores:  36  37  38  39  40  41  42  43  44
   TASKS:  64 THREADS: 9    THIS:  14     Host:adag013    Cores:  54  55  56  57  58  59  60  61  62
[....]
```

### 1.3) *Optional:* try the [likwid tool](https://github.com/RRZE-HPC/likwid) by RRZE

A very useful (and portable!) performance toolbox by RRZE


```
#install
wget http://ftp.fau.de/pub/likwid/likwid-stable.tar.gz

tar xzf likwid-stable.tar.gz 

cd likwid-5.2.2/ 

sed -i ‘s/ACCESSMODE\s*=.*/ACCESSMODE = direct/’ config.mk

module load gcc

make PREFIX=$HOME/sw/likwid 

make install DESTDIR=$HOME/sw/likwid

#run
export PATH=${PATH}:$HOME/sw/likwid/bin

likwid-topology #HW topology (like lstopo, ...) 

likwid-bench #HW microbenchmarks 

likwid-perfctr #measure application performance #might require additional privileges 

```



## Part 2: Exploring a (newly developed) code: *How do we perform / scale?*

### 2.1) Overview:

#### Example Code: 


  * basic Jacobi solver for elliptic PDE in 2D (by courtesy of G. Hager, RRZE) 

  * or use your own
  
  * or let ChatGPT create one for you 


#### Tasks: 

  * explore strong and weak scaling behaviour across LUMI nodes with the help of the C or Fortran version of the code

  * produce scaling plots

  * *optional:* assess hybrid MPI-OpenMP performance, overhead (vs. plain MPI)

![scaling example](images/scaling_strong.png)



### 2.2) Build code on LUMI (C example)

```
git clone https://gitlab.mpcdf.mpg.de/mjr/nomad-summer-2023.git

cd nomad-summer-2023/Jacobi_hybrid/C-hyb-jacobi/

module load gcc

make
```

### 2.3) Run strong scaling and weak scaling experiments

Prepare and submit hybrid MPI+OpenMP batch jobs on N nodes
(N=1,2,4,8), [building upon a LUMI sample slurm script](https://docs.lumi-supercomputer.eu/runjobs/scheduled-jobs/lumic-job/)


#### Hints:


  * command reads: `srun ./jacobi.exe < input` 

  * start out with
```
#SBATCH --ntasks-per-node=16
#SBATCH --cpus-per-task=8
```

  * beware of jobs reading modified input file!

  * start with **short tests**: use 100 (instead of 1000) steps, see last line in `input` file 
  
  * always try to specify realistic (short) runtime, not the maximum
    24:00:00 !!!
    
  * use reasonably small node counts on LUMI (1,2,4)
  



#### Strong scaling: 

  * run with matrix size: 1000x1000 (see lines 1-2 in file input) 

  * run with matrix size: 40000x40000 (modify lines 1-2 in file input)

  * useful for strong-scaling sequence of jobs: override relevant sbatch resource parameters on command line
```
sbatch --job-name=jacobi_01n_16tpn --nodes=1 submit_hybrid.slrm
sbatch --job-name=jacobi_02n_16tpn --nodes=2 submit_hybrid.slrm 
sbatch --job-name=jacobi_04n_16tpn --nodes=4 submit_hybrid.slrm 

sbatch --job-name=jacobi_04n_64tpn --nodes=4  --ntasks-per-node=64 --cpus-per-task=2 submit_hybrid.slrm
```

#### Weak scaling: 

  * increase matrix size from 40000x 40000 along with node count *(think: ~ N, or ~ N x N ?)*

  * useful for weak-scaling sequence of jobs: *dynamically* generate input file and make it job-specific:
  
```
#!/bin/bash -l 
#SBATCH --job-name=jacobi_02n_16tpn  # Job name
#SBATCH --time=00:10:00

# ...
cat <<EOF > input.${SLURM_JOB_ID}
80000
40000
0.8
1.0
1e-20
100
EOF

srun ./jacobi.exe < input.${SLURM_JOB_ID}
```


### 2.4) *Optional:* advanced performance analysis:

* [LUMI CRAYPAT](https://docs.lumi-supercomputer.eu/development/profiling/perftools/)

* Laptops / Intel machines: [Intel oneAPI: APS, Vtune/Advisor](https://www.intel.com/content/www/us/en/developer/tools/oneapi/overview.html)
