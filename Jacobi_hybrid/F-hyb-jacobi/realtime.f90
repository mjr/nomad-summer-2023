module RealTime
    implicit none
    contains
    double precision function GetRealTime()
        CHARACTER(len=12):: date, time
        CALL date_and_time(date, time)
!       returns time as hhmmss.xxx <=> about 1 ms exact
        read(time, '(f10.3)') GetRealTime
        return 
    end function
end module RealTime
      
